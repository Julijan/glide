﻿using System;
using UnityEngine;
using UnityEngine.U2D;

public class LandedGooController : MonoBehaviour {
    private Vector2 Normal;
    private Vector2 CollisionPos;
    public Vector2 Tangent;
    public bool IgnoreTangent = false;

    public AudioSource AudioSource;
    public AudioClip LandSound;

    public void Init(Collision2D other) {
        Normal = other.contacts[0].normal;
        CollisionPos = other.contacts[0].point;

        Vector3 t1 = Vector3.Cross(Normal, Vector3.forward);
        Vector3 t2 = Vector3.Cross(Normal, Vector3.up);
        if (t1.magnitude > t2.magnitude) {
            Tangent = t1;
        }
        else {
            Tangent = t2;
        }

        float angle = Vector2.Angle(-Vector2.up, Tangent);
        float angle2 = Vector2.Angle(-Vector2.up, -Tangent);

        if (Mathf.Abs(angle - angle2) < 15f) {
            IgnoreTangent = true;
        }

        if (angle > angle2) {
            Tangent = -Tangent;
        }

        transform.position = CollisionPos;

        AudioSource.PlayOneShot(LandSound);
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(CollisionPos, CollisionPos + Normal);
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(CollisionPos, CollisionPos + Tangent);
    }

    private void Update() {
        if (Input.GetMouseButtonDown(1)) {
            Destroy(gameObject);
        }
    }
}