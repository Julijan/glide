﻿using UnityEngine;

public class RayShotterDown : MonoBehaviour {
    private Vector2 Normal;
    private Vector2 CollisionPos;
    private Vector2 Tangent;

    public GameObject Spawn;

    private void FixedUpdate() {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up);

        // If it hits something...
        if (hit.collider != null) {
            Normal = hit.normal;
            CollisionPos = hit.point;

            Vector3 t1 = Vector3.Cross(Normal, Vector3.forward);
            Vector3 t2 = Vector3.Cross(Normal, Vector3.up);
            if (t1.magnitude > t2.magnitude) {
                Tangent = t1;
            }
            else {
                Tangent = t2;
            }

            float angle = Vector2.Angle(-Vector2.up, Tangent);
            float angle2 = Vector2.Angle(-Vector2.up, -Tangent);

            if (angle > angle2) {
                Tangent = -Tangent;
            }

            Spawn.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 90f));
            Spawn.transform.position = CollisionPos;
        }
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(CollisionPos, CollisionPos + Normal);
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(CollisionPos, CollisionPos + Tangent);
    }
}