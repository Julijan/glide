﻿using UnityEngine;

public class UIController : MonoBehaviour {
    public ProgressController LifeProgressBar;
    public ProgressController WeaponProgressBar;

    public static UIController Instance;

    void Awake() {
        //if we don't have an [_instance] set yet
        if (!Instance)
            Instance = this;
        //otherwise, if we do, kill this thing
        else
            Destroy(this.gameObject);


        DontDestroyOnLoad(this.gameObject);
    }
}