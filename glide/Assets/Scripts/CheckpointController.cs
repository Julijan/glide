﻿using System.Collections.Generic;
using UnityEngine;

public class CheckpointController : MonoBehaviour {
    public List<GameObject> particles;

    [SerializeField] private int CheckPointIndexThis;

    public GameObject SpawnPoint;

    public int CheckPointIndex {
        get {
            foreach (var particle in particles) {
                particle.SetActive(true);
            }

            return CheckPointIndexThis;
        }
    }
}