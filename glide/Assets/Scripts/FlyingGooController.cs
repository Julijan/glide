﻿using UnityEngine;

public class FlyingGooController : MonoBehaviour {
    public LandedGooController LandedGooController;

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.CompareTag("Floor")) {
            Destroy(gameObject);

            LandedGooController temp = Instantiate(LandedGooController, other.contacts[0].point, Quaternion.identity);
            temp.Init(other);
        }
    }
}