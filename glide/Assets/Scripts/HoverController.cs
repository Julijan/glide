﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Lessons:
 * 1. Scripts are mini-programs that operate on a GameObject in your scene.
 * 2. How to get a component of the GameObject this script is attached to?
 * 3. How to modify that component over time.
 * 4. FIX IN CLASS: There is actually a shortcut to get the transform of the GameObject.
 **/


public class HoverController : MonoBehaviour {

    // Public variables show up in the Inspector
    public Vector3 rotateSpeed = new Vector3 (0.0F, 0.0F, 10.0F);
    public Vector3 wobbleAmount = new Vector3 (0.1F, 0.1F, 0.1F);
    public Vector3 wobbleSpeed = new Vector4 (0.2F, 0.2F, 0.2F);

    public float rotationWobbleAmount = 0.5f;
    public float rotationWobbleSpeed = 0.5f;
    
    // Private variables do not show up in the Inspector
    private Transform _tr;
    private Vector3 _basePosition;
    private Vector3 _noiseIndex = new Vector3();
    private float _rotationNoiseIndex;

    private float rotateOffset;


    // Use this for initialization
    void Start () {
		
        // https://docs.unity3d.com/ScriptReference/GameObject.GetComponent.html
        _tr = GetComponent ("Transform") as Transform;

        _basePosition = _tr.position;

        _noiseIndex.x = Random.value;
        _noiseIndex.y = Random.value;
        _noiseIndex.z = Random.value;

        _rotationNoiseIndex = Random.value;
    }
    
    
    Quaternion ClampRotationAroundZAxis(Quaternion q) {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;
             
        float angleZ = 2.0f * Mathf.Rad2Deg * Mathf.Atan (q.z);
        angleZ = Mathf.Clamp (angleZ, -10f, 10f);
        q.z = Mathf.Tan (0.5f * Mathf.Deg2Rad * angleZ);
        
        return q;
    }
	
    // Update is called once per frame
    void Update () {

        // 1. ROTATE
        // Rotate the cube by RotateSpeed, multiplied by the fraction of a second that has passed.
        // In other words, we want to rotate by the full amount over 1 second
        float rotate_offset = Mathf.PerlinNoise(_rotationNoiseIndex, 0) - 0.5F;
        _tr.Rotate (Time.deltaTime * rotateSpeed * rotate_offset * rotationWobbleAmount);
        
        //_tr.localEulerAngles = new Vector3(_tr.localEulerAngles.x, _tr.localEulerAngles.y, Mathf.Clamp(_tr.localEulerAngles.z, -10.0f, 10.0f));
        //Vector3 currentRotation = transform.localRotation.eulerAngles;
        //currentRotation.z = Mathf.Clamp(currentRotation.z, -10f, 10f);
        transform.localRotation = ClampRotationAroundZAxis(transform.localRotation); //Quaternion.Euler(currentRotation));


        // 2. WOBBLE
        // Calculate how much to offset the cube from it's base position using PerlinNoise
        Vector3 offset = new Vector3 ();
        offset.x = Mathf.PerlinNoise (_noiseIndex.x, 0) - 0.5F;
        offset.y = Mathf.PerlinNoise (_noiseIndex.y, 0) - 0.5F;
        offset.z = Mathf.PerlinNoise (_noiseIndex.z, 0) - 0.5F;

        offset.Scale(wobbleAmount);

        // Set the position to the BasePosition plus the offset
        transform.position = _basePosition + offset;

        // Increment the NoiseIndex so that we get a new Noise value next time.
        _noiseIndex += wobbleSpeed * Time.deltaTime;
        _rotationNoiseIndex += rotationWobbleSpeed * Time.deltaTime;
    }
}