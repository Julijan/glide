﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MenuButtonController : MonoBehaviour {
    public TMP_Text textMesh;

    public Color normalColor;
    public Color hoverColor;
    

    public void PointerEnter() {
        textMesh.color = hoverColor;
    }

    public void PointerExit() {
        textMesh.color = normalColor;
    }
}
