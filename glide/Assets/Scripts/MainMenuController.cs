﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {
    public GameObject UI;

    private void Start() {
        MusicPlayer._instance.AudioSource.Stop();
        MusicPlayer._instance.AudioSource.clip = MusicPlayer._instance.GameplaySound;
        MusicPlayer._instance.AudioSource.Play();
    }

    public void StartClick() {
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadSceneAsync("Level0");
        GameObject ui = (GameObject) Instantiate(UI);
        DontDestroyOnLoad(ui);
    }

    public void ExitClick() {
        Application.Quit();
    }
}