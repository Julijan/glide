﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class ProgressController : MonoBehaviour {

    public Image image;

    void Start() {
        Assert.IsNotNull(image);
        SetCharge(1.0f);
    }

    public void SetCharge(float value) {
        float v = Mathf.Clamp01(value);
        image.material.SetFloat("_Progress", v);
    }
}
