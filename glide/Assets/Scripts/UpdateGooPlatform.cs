﻿using UnityEngine;
using UnityEngine.U2D;

[ExecuteInEditMode]
public class UpdateGooPlatform : MonoBehaviour {
    public SpriteShapeController NormalPlatform;
    public SpriteShapeController GooPlatform;

    private void Start() {
        GooPlatform.spline.Clear();
        for (int i = 0; i < NormalPlatform.spline.GetPointCount(); i++) {
            GooPlatform.spline.InsertPointAt(i, NormalPlatform.spline.GetPosition(i));
            GooPlatform.spline.SetTangentMode(i, NormalPlatform.spline.GetTangentMode(i));
            GooPlatform.spline.SetLeftTangent(i, NormalPlatform.spline.GetLeftTangent(i));
            GooPlatform.spline.SetRightTangent(i, NormalPlatform.spline.GetRightTangent(i));
        }

        GooPlatform.RefreshSpriteShape();
        GooPlatform.UpdateSpriteShapeParameters();
    }

    void Update() {
        if (!Application.isPlaying) {
            GooPlatform.spline.Clear();
            for (int i = 0; i < NormalPlatform.spline.GetPointCount(); i++) {
                GooPlatform.spline.InsertPointAt(i, NormalPlatform.spline.GetPosition(i));
                GooPlatform.spline.SetTangentMode(i, NormalPlatform.spline.GetTangentMode(i));
                GooPlatform.spline.SetLeftTangent(i, NormalPlatform.spline.GetLeftTangent(i));
                GooPlatform.spline.SetRightTangent(i, NormalPlatform.spline.GetRightTangent(i));
            }

            GooPlatform.RefreshSpriteShape();
            GooPlatform.UpdateSpriteShapeParameters();
        }
    }
}