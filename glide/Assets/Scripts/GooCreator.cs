﻿using System;
using UnityEngine;

public class GooCreator : MonoBehaviour {
    private Camera Camera;
    public GameObject Player;
    private Vector3 Vector;
    public Rigidbody2D Acid;
    public Transform OriginOfAcid;

    public int MaxGooCap = 10;
    public int CurrentGoo = 10;

    public AudioSource AudioSource;
    public AudioClip ThrowSound;

    private void Start() {
        Camera = Camera.main;
    }

    private void Update() {
        Vector3 worldPos = Camera.ScreenToWorldPoint(Input.mousePosition);
        Vector = new Vector2(worldPos.x, worldPos.y) -
                 new Vector2(Player.transform.position.x, Player.transform.position.y);
        Vector = Vector.normalized * 6.3f;
        if (Input.GetMouseButtonDown(0)) {
            ThrowAcid();
        }
        else if (Input.GetMouseButtonDown(1)) {
            SuckAcidIn();
        }
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(Player.transform.position, Player.transform.position + Vector);
    }

    void ThrowAcid() {
        CurrentGoo--;
        if (CurrentGoo > 0) {
            AudioSource.PlayOneShot(ThrowSound);
            UIController.Instance.WeaponProgressBar.SetCharge((float) CurrentGoo / MaxGooCap);
            Rigidbody2D acid = Instantiate(Acid, OriginOfAcid.position, Quaternion.identity);
            acid.AddForce(Vector, ForceMode2D.Impulse);
        }
    }

    void SuckAcidIn() {
        CurrentGoo = MaxGooCap;
        UIController.Instance.WeaponProgressBar.SetCharge((float) CurrentGoo / MaxGooCap);
    }
}