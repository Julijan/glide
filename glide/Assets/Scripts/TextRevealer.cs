﻿using System.Collections;
using System.Collections.Generic;
using KoganeUnityLib;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TextRevealer : MonoBehaviour {
    public TMP_Typewriter m_typewriter;
    public float m_speed;
    public Image FadeInImage;
    public List<Image> Decoration;

    private void Start() {
        m_typewriter.gameObject.SetActive(false);
        FadeInImage.canvasRenderer.SetAlpha(0.0f);
        FadeInImage.CrossFadeAlpha(1, 2f, false);

        foreach (Image image in Decoration) {
            image.canvasRenderer.SetAlpha(0.0f);
            image.CrossFadeAlpha(1, 2f, false);
        }
        
        StartCoroutine(PlayText());
        MusicPlayer._instance.AudioSource.Stop();
        MusicPlayer._instance.AudioSource.clip = MusicPlayer._instance.LoadingScreenSound;
        MusicPlayer._instance.AudioSource.Play();
    }

    IEnumerator PlayText() {
        yield return new WaitForSeconds(2f);
        SceneManager.UnloadSceneAsync(LoadingManager.LevelToUnload);
        m_typewriter.gameObject.SetActive(true);
        m_typewriter.Play
        (
            text: LoadingManager.TextToDisplay,
            speed: m_speed,
            onComplete: () => { StartCoroutine(LoadLevel()); }
        );
    }

    IEnumerator LoadLevel() {
        yield return new WaitForSeconds(2f);
        m_typewriter.gameObject.SetActive(false);
        SceneManager.LoadSceneAsync(LoadingManager.LevelToLoad, LoadSceneMode.Additive);
        FadeInImage.canvasRenderer.SetAlpha(1.0f);
        FadeInImage.CrossFadeAlpha(0, 2f, false);
        
        foreach (Image image in Decoration) {
            image.canvasRenderer.SetAlpha(1.0f);
            image.CrossFadeAlpha(0, 2f, false);
        }
        
        MusicPlayer._instance.AudioSource.Stop();
        MusicPlayer._instance.AudioSource.clip = MusicPlayer._instance.GameplaySound;
        MusicPlayer._instance.AudioSource.Play();
        yield return new WaitForSeconds(2f);
        SceneManager.UnloadSceneAsync("LoadingScene");
    }
}