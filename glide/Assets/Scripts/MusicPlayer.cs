﻿using UnityEngine;

public class MusicPlayer : MonoBehaviour {
    public static MusicPlayer _instance;

    public AudioSource AudioSource;
    public AudioClip GameplaySound;
    public AudioClip LoadingScreenSound;

    void Awake() {
        //if we don't have an [_instance] set yet
        if (!_instance)
            _instance = this;
        //otherwise, if we do, kill this thing
        else
            Destroy(this.gameObject);


        DontDestroyOnLoad(this.gameObject);
    }

    public void PlayOneShot(AudioClip audioClip) {
        AudioSource.PlayOneShot(audioClip);
    }
}